<?php

namespace WarehouseApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    private static $testedItem = [
        'name' => 'TEST TEST TEST',
        'amount' => 100
    ];

    public function testBadRequestError()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/item/'
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        $data = \json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($data);

        $this->assertArrayHasKey('error', $data);
    }

    /**
     * @depends testBadRequestError
     */
    public function testAddItem()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/item/',
            self::$testedItem
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $data = \json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($data);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('amount', $data);

        self::$testedItem['id'] = $data['id'];

        $this->assertEquals(self::$testedItem['name'], $data['name']);
        $this->assertEquals(self::$testedItem['amount'], $data['amount']);
    }

    /**
     * @depends testAddItem
     */
    public function testUpdateItem()
    {
        $client = static::createClient();

        $updatedItem = [
            'name' => 'TEST ?? TEST ?? TEST',
            'amount' => 5
        ];

        $client->request(
            'PUT',
            '/api/item/'.self::$testedItem['id'].'/',
            $updatedItem
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $data = \json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($data);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('amount', $data);

        self::$testedItem = $updatedItem;
        self::$testedItem['id'] = $data['id'];

        $this->assertEquals(self::$testedItem['id'], $data['id']);
        $this->assertEquals(self::$testedItem['name'], $data['name']);
        $this->assertEquals(self::$testedItem['amount'], $data['amount']);
    }

    /**
     * @depends testUpdateItem
     */
    public function testDeleteItem()
    {
        $client = static::createClient();

        $client->request(
            'DELETE',
            '/api/item/'.self::$testedItem['id'].'/'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $data = \json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($data);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('amount', $data);

        $this->assertEquals(self::$testedItem['id'], $data['id']);
        $this->assertEquals(self::$testedItem['name'], $data['name']);
        $this->assertEquals(self::$testedItem['amount'], $data['amount']);
    }

    /**
     * @depends testDeleteItem
     */
    public function testInStock()
    {
        // Dodawanie elementu
        $client = static::createClient();

        $item = [
            'name' => 'TEST IN STOCK',
            'amount' => 5
        ];

        $client->request(
            'POST',
            '/api/item/',
            $item
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $item = \json_decode($client->getResponse()->getContent(), true);

        // Pobieranie elementow w magazynie
        $client->request(
            'GET',
            '/api/items/inStock/'
        );

        $data = \json_decode($client->getResponse()->getContent(), true);

        // Usuwanie elementu
        $client->request(
            'DELETE',
            '/api/item/'.$item['id'].'/'
        );

        // Sprawdzenie czy element istnial przed usunieciem
        $this->assertNotEmpty($data);

        // Szukanie elementu
        $existing = [];
        foreach ($data as $existingItem) {
            if ($existingItem['id'] == $item['id']) {
                $existing = $existingItem;
                break;
            }
        }

        $this->assertEquals($item, $existing);
    }

    /**
     * @depends testDeleteItem
     */
    public function testOutOfStock()
    {
        // Dodawanie elementu
        $client = static::createClient();

        $item = [
            'name' => 'TEST OUT OF STOCK',
            'amount' => 0
        ];

        $client->request(
            'POST',
            '/api/item/',
            $item
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $item = \json_decode($client->getResponse()->getContent(), true);

        // Pobieranie elementow w magazynie
        $client->request(
            'GET',
            '/api/items/outOfStock/',
            $item
        );

        $data = \json_decode($client->getResponse()->getContent(), true);

        // Usuwanie elementu
        $client->request(
            'DELETE',
            '/api/item/'.$item['id'].'/'
        );

        // Sprawdzenie czy element istnial przed usunieciem
        $this->assertNotEmpty($data);

        // Szukanie elementu
        $existing = [];
        foreach ($data as $existingItem) {
            if ($existingItem['id'] == $item['id']) {
                $existing = $existingItem;
                break;
            }
        }

        $this->assertEquals($item, $existing);
    }

    /**
     * @depends testDeleteItem
     */
    public function testMoreThan5()
    {
        // Dodawanie elementu
        $client = static::createClient();

        $item = [
            'name' => 'TEST MORE THANT 5',
            'amount' => 6
        ];

        $client->request(
            'POST',
            '/api/item/',
            $item
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $item = \json_decode($client->getResponse()->getContent(), true);

        // Pobieranie elementow w magazynie
        $client->request(
            'GET',
            '/api/items/inStock/?moreThanAmount=5'
        );

        $data = \json_decode($client->getResponse()->getContent(), true);

        // Usuwanie elementu
        $client->request(
            'DELETE',
            '/api/item/'.$item['id'].'/'
        );

        // Sprawdzenie czy element istnial przed usunieciem
        $this->assertNotEmpty($data);

        // Szukanie elementu
        $existing = [];
        foreach ($data as $existingItem) {
            if ($existingItem['id'] == $item['id']) {
                $existing = $existingItem;
                break;
            }
        }

        $this->assertEquals($item, $existing);
    }

    /**
     * @depends testDeleteItem
     */
    public function testNoExistItemMoreThan5()
    {
        // Dodawanie elementu
        $client = static::createClient();

        $item = [
            'name' => 'TEST MORE THANT 5',
            'amount' => 5
        ];

        $client->request(
            'POST',
            '/api/item/',
            $item
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $item = \json_decode($client->getResponse()->getContent(), true);

        // Pobieranie elementow w magazynie
        $client->request(
            'GET',
            '/api/items/inStock/?moreThanAmount=5'
        );

        // checking status 503 - thats means no data (Service Unavailable)
        if ($client->getResponse()->getStatusCode() == 503) {
            return;
        }

        $data = \json_decode($client->getResponse()->getContent(), true);

        // Usuwanie elementu
        $client->request(
            'DELETE',
            '/api/item/'.$item['id'].'/'
        );

        // Sprawdzenie czy element istnial przed usunieciem
        $this->assertNotEmpty($data);

        // Szukanie elementu
        $existing = [];
        foreach ($data as $existingItem) {
            if ($existingItem['id'] == $item['id']) {
                $existing = $existingItem;
                break;
            }
        }

        $this->assertEquals([], $existing);
    }
}
