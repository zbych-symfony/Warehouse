<?php
/**
 * Usługa pobierająca dane dot. produktów w magazynie
 */
namespace WarehouseApiBundle\Service;

use WarehouseApiBundle\Entity\BasicWarehouseItemInterface;
use WarehouseApiBundle\Entity\Item;
use Doctrine\ORM\EntityManager;

class Warehouse implements BasicWarehouseInterface, StateOfWarehouseInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \WarehouseApiBundle\Repository\ItemRepository|\Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * Warehouse constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('WarehouseApiBundle:Item');
    }

    /**
     * Dodaje element do magazynu
     *
     * @param BasicWarehouseItemInterface $item
     * @return BasicWarehouseItemInterface
     */
    public function addItem(BasicWarehouseItemInterface $item) : BasicWarehouseItemInterface
    {
        $this->em->persist($item);
        $this->em->flush();

        return $item;
    }

    /**
     * Aktualizuje element magazynu
     *
     * @param int $id
     * @param BasicWarehouseItemInterface $item
     * @return BasicWarehouseItemInterface
     */
    public function updateItem(int $id, BasicWarehouseItemInterface $item) : BasicWarehouseItemInterface
    {
        $current = $this->repository->find($id);
        $current->setName($item->getName());
        $current->setAmount($item->getAmount());
        $this->em->flush();
        return $current;
    }

    /**
     * Usuwa element z magazynu
     *
     * @param int $id
     * @return BasicWarehouseItemInterface
     */
    public function removeItem(int $id) : BasicWarehouseItemInterface
    {
        $item = $this->repository->find($id);
        $this->em->remove($item);
        $this->em->flush();

        $item->setId($id);

        return $item;
    }

    /**
     * @return BasicWarehouseItemInterface[]
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * Zwraca produkty których ilość jest większa od wskazanej w $amount
     *
     * @param int $amount
     * @return array
     */
    public function getAllMoreThanAmount(int $amount) : array
    {
        $items = $this->repository->getMoreThanAmount($amount);
        return $items->count() ? $items->toArray() : [];
    }

    /**
     * Zwraca produkty których ilość jest mniejsza od wskazanej w $amount
     *
     * @param int $amount
     * @return array
     */
    public function getAllLessThanAmount(int $amount) : array
    {
        $items = $this->repository->getLessThanAmount($amount);
        return $items->count() ? $items->toArray() : [];
    }

    /**
     * Zwraca wszystkie produkty które są dostępne w magazynie
     * (Ilość jest większa od 0)
     *
     * @return array
     */
    public function getAllInStock() : array
    {
        return $this->getAllMoreThanAmount(0);
    }

    /**
     * Zwraca wszystkie produkty które nie są dostępne w magazynie
     * (Ilość jest mniejsza od 1)
     *
     * @return array
     */
    public function getAllOutOfStock() : array
    {
        return $this->getAllLessThanAmount(1);
    }
}