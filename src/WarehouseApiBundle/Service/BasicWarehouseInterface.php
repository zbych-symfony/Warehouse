<?php
/**
 * Podstawowy interfejs usług zarządzającymi magazynem
 *
 * Zawiera podstawowe metody pozwalające na dodawanie, aktualizacje oraz usuwanie
 * elementów magazynu
 *
 * Interface WarehouseInterface
 * @package WarehouseApiBundle\Service
 */

namespace WarehouseApiBundle\Service;

use WarehouseApiBundle\Entity\BasicWarehouseItemInterface;

interface BasicWarehouseInterface
{
    /**
     * Dodaje element do magazynu
     *
     * @param BasicWarehouseItemInterface $item
     * @return BasicWarehouseItemInterface
     */
    public function addItem(BasicWarehouseItemInterface $item) : BasicWarehouseItemInterface;

    /**
     * Aktualizuje element magazynu
     *
     * @param int $id
     * @param BasicWarehouseItemInterface $item
     * @return BasicWarehouseItemInterface
     */
    public function updateItem(int $id, BasicWarehouseItemInterface $item) : BasicWarehouseItemInterface;

    /**
     * Usuwa element z magazynu
     *
     * @param int $id
     * @return BasicWarehouseItemInterface
     */
    public function removeItem(int $id) : BasicWarehouseItemInterface;
}