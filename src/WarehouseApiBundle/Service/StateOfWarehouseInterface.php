<?php
/**
 * Deklaruje podstawowe metody pozwalające na zwracanie stanu produktów w magazynie
 */

namespace WarehouseApiBundle\Service;


interface StateOfWarehouseInterface
{
    /**
     * Zwraca produkty których ilość jest większa od wskazanej w $amount
     *
     * @param int $amount
     * @return array
     */
    public function getAllMoreThanAmount(int $amount) : array;

    /**
     * Zwraca produkty których ilość jest mniejsza od wskazanej w $amount
     *
     * @param int $amount
     * @return array
     */
    public function getAllLessThanAmount(int $amount) : array;

    /**
     * Zwraca wszystkie produkty które są dostępne w magazynie
     * (Ilość jest większa od 0)
     *
     * @return array
     */
    public function getAllInStock() : array;

    /**
     * Zwraca wszystkie produkty które nie są dostępne w magazynie
     * (Ilość jest mniejsza od 1)
     *
     * @return array
     */
    public function getAllOutOfStock() : array;
}