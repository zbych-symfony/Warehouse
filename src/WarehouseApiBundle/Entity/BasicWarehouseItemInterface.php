<?php
/**
 * Interfejs prostego produktu magazynu
 */
namespace WarehouseApiBundle\Entity;


interface BasicWarehouseItemInterface
{
    /**
     * Zwraca unikatowy identyfikator elementu magazynu
     *
     * @return integer
     */
    public function getId(): int;

    /**
     * Zwraca nazwę elementu magazynu
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Zwraca ilość dostępnego produktu w magazynie
     *
     * @return int
     */
    public function getAmount(): int;
}