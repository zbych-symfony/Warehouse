<?php
/**
 * Element produktu w magazynie
 */
namespace WarehouseApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="WarehouseApiBundle\Repository\ItemRepository")
 */
class Item implements BasicWarehouseItemInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @param int $id
     *
     * @return Item
     */
    public function setId(int $id): Item
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Zwraca unikatowy identyfikator elementu magazynu
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Zwraca nazwę elementu magazynu
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Item
     */
    public function setAmount(int $amount): Item
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Zwraca ilość dostępnego produktu w magazynie
     *
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}

