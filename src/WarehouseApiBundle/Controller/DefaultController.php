<?php

namespace WarehouseApiBundle\Controller;

use FOS\RestBundle\View\View;
use WarehouseApiBundle\Entity\BasicWarehouseItemInterface;
use WarehouseApiBundle\Entity\Item;
use WarehouseApiBundle\Service\Warehouse;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DefaultController extends FOSRestController
{
    /**
     * @var Warehouse
     */
    private $warehouse;

    /**
     * DefaultController constructor.
     * @param Warehouse $warehouse
     */
    public function __construct(Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
    }

    /**
     * @Rest\Get("/items/")
     * @Rest\View()
     *
     * @return array
     */
    public function getItemsAction()
    {
        $items = $this->warehouse->getAll();

        if ($items) {
            return $items;
        } else {
            throw new HttpException(503, "The warehouse is empty");
        }
    }

    /**
     * @Rest\Get("/items/inStock/")
     * @Rest\QueryParam(name="moreThanAmount", default="0", requirements="\d+",
     *     description="Określa większą niż wskazaną wartosc jaka może wystąpić produktów w magazynie")
     *
     * @Rest\View()
     * @param int $moreThanAmount Określa większą niż wskazaną wartość jaka może wystąpić produktów w magazynie
     *
     * @return array
     */
    public function getItemsInStockAction($moreThanAmount = 0)
    {
        $items = $this->warehouse->getAllMoreThanAmount($moreThanAmount);

        if ($items) {
            return $items;
        } else {
            throw new HttpException(503, "No results");
        }
    }

    /**
     * @Rest\Get("/items/outOfStock/")
     * @Rest\View()
     *
     * @return array
     */
    public function getItemsOutOfStockAction()
    {
        $items = $this->warehouse->getAllOutOfStock();

        if ($items) {
            return $items;
        } else {
            throw new HttpException(503, "No results");
        }
    }

    /**
     * Dodaje nowy element do magazynu
     *
     * @Rest\Post("/item/")
     * @Rest\RequestParam(name="name", requirements=".{0,200}", description="Nazwa elementu dodawanego do magazynu")
     * @Rest\RequestParam(name="amount", requirements="\d+", default="1", description="Ilość jaka ma zostać dodana")
     * @Rest\View()
     *
     * @param string $name
     * @param int $amount
     *
     * @return View
     */
    public function newItemAction($name, $amount = 1)
    {
        $item = new Item();
        $item->setName($name);
        $item->setAmount($amount);

        return View::create($this->warehouse->addItem($item), 201);
    }

    /**
     * @Rest\Put("/item/{id}/")
     * @Rest\RequestParam(name="name", requirements=".{0,200}", description="Nazwa elementu dodawanego do magazynu")
     * @Rest\RequestParam(name="amount", requirements="\d+", default="1", description="Ilość jaka ma zostać dodana")
     * @Rest\View()
     *
     * @param $id
     * @param $name
     * @param int $amount
     *
     * @return BasicWarehouseItemInterface
     */
    public function editItemAction($id, $name, $amount = 1)
    {
        $item = new Item();
        $item->setName($name);
        $item->setAmount($amount);

        return $this->warehouse->updateItem($id, $item);
    }

    /**
     * @Rest\Delete("/item/{id}/")
     * @Rest\View()
     * @param int $id
     *
     * @return BasicWarehouseItemInterface
     */
    public function removeItemAction(int $id)
    {
        return $this->warehouse->removeItem($id);
    }

}
