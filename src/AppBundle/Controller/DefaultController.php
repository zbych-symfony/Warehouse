<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $api = $this->container->get('api.warehouse');

        try {
            return $this->render('@App/default/index.html.twig', [
                'in_stock' => $api->getAllItemsInStock(),
                'out_of_stock' => $api->getAllItemsOutOfStock(),
                'more_than_5' => $api->getAllItemsMoreThen5(),
            ]);
        } catch (\Exception $e) {
            return $this->render('@App/error/api.twig');
        }
    }
}
