<?php

namespace AppBundle\Service\WarehouseApiClient;

use AppBundle\Entity\Item;
use AppBundle\Service\Factory\ItemFactory;
use GuzzleHttp\Exception\ServerException;
use Psr\Http\Message\ResponseInterface;

class WarehouseApi
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var ItemFactory
     */
    private $itemFactory;

    public function __construct(Client $client, ItemFactory $itemFactory)
    {
        $this->client = $client;
        $this->itemFactory = $itemFactory;
    }

    /**
     * Zwraca listę wszystkich produktów
     *
     * @return Item|array
     */
    public function getAllItems()
    {
        try {
            return $this->parseResponse(
                $this->client->get('items/')
            );
        } catch (ServerException $e) {
            if ($e->getCode() == 503) {
                return [];
            } else {
                throw $e;
            }
        }
    }

    /**
     * Zwraca listę wszystkich produktów które są na stanie
     *
     * @return Item|array
     */
    public function getAllItemsInStock()
    {
        try {
            return $this->parseResponse(
                $this->client->get('items/inStock/')
            );
        } catch (ServerException $e) {
            if ($e->getCode() == 503) {
                return [];
            } else {
                throw $e;
            }
        }
    }

    /**
     * Zwraca listę wszystkich produktów które nie znajdują się na stanie
     *
     * @return Item|array
     */
    public function getAllItemsOutOfStock()
    {
        try {
            return $this->parseResponse(
                $this->client->get('items/outOfStock/')
            );
        } catch (ServerException $e) {
            if ($e->getCode() == 503) {
                return [];
            } else {
                throw $e;
            }
        }
    }

    /**
     * Zwraca listę wszystkich produktów których na stanie jest powyżej 5
     *
     * @return Item|array
     */
    public function getAllItemsMoreThen5()
    {
        try {
            return $this->parseResponse(
                $this->client->get('items/inStock/', [
                    'query' => [
                        'moreThanAmount' => 5
                    ]
                ])
            );
        } catch (ServerException $e) {
            if ($e->getCode() == 503) {
                return [];
            } else {
                throw $e;
            }
        }
    }

    /**
     * Dodaje element do baze api
     *
     * @param Item $item
     * @return Item|array
     */
    public function addItem(Item $item)
    {
        return $this->parseResponse(
            $this->client->post('item/', [
                'form_params' => [
                    'name' => $item->name,
                    'amount' => $item->amount
                ]
            ])
        );
    }

    /**
     * Aktualizuje element
     *
     * @param int $id
     * @param Item $item
     * @return Item|array
     */
    public function updateItem(int $id, Item $item)
    {
        return $this->parseResponse(
            $this->client->put("item/$id/", [
                'form_params' => [
                    'name' => $item->name,
                    'amount' => $item->amount
                ]
            ])
        );
    }

    /**
     * Usuwa element
     *
     * @param int $id
     * @return Item|array
     */
    public function removeItem(int $id)
    {
        return $this->parseResponse(
            $this->client->delete("item/$id/")
        );
    }

    /**
     * Faormatuje odpowiedź zwróconą z Api na tablice danych - obiektów Item
     *
     * @param ResponseInterface $response
     * @return \AppBundle\Entity\Item|array
     */
    protected function parseResponse(ResponseInterface $response)
    {
        return $this->toItemEntity(\json_decode($response->getBody()->getContents()));
    }

    /**
     * Zamienia tablice elementów, lub pojedynczy element na obiekt \AppBundle\Entity\Item
     *
     * @param array|\stdClass $data
     * @return \AppBundle\Entity\Item|array
     */
    protected function toItemEntity($data)
    {
        $factory = $this->itemFactory;

        if (is_array($data)) {
            $items = [];

            foreach ($data as $key => $item) {
                $items[$key] = $this->toItemEntity($item);
            }

            return $items;
        } else {
            return $factory($data);
        }
    }

}