<?php
namespace AppBundle\Service\Factory;

use AppBundle\Entity\Item;

class ItemFactory
{
    public function __invoke($data = [])
    {
        $item = new Item();

        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        return $item;
    }
}