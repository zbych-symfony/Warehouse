<?php

namespace AppBundle\Entity;

class Item
{
    /**
     * @var int
     */
    public $id = 0;

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var int
     */
    public $amount = 0;
}

